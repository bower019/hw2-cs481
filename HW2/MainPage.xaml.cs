﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW2
{
    public partial class MainPage : ContentPage
    {
        int currentstate = 1;
       

        public MainPage()
        {
            InitializeComponent();
            Wipe(this, null);
        }

        void Num(object send, EventArgs c)
        {
            Button button = (Button)send;
            string press = button.Text;

            if (this.resultText.Text == "0" || currentstate < 0)
            {
                this.resultText.Text = "";
                if (currentstate < 0)
                    currentstate *= -1;
            }

            this.resultText.Text += press;

          
        }



        void Wipe(object sender, EventArgs c)
        {

            currentstate = 1;
            this.resultText.Text = "0";
        }

       
        }
    }



